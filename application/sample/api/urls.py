from django.urls import path
from api.views.ping_view import PingView

urlpatterns = [
    path('ping', PingView.as_view(), name='pingpong'),
]